import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export class FilterPipe implements PipeTransform{

    transform(productos: any, term: any): any {
        
        if(term === undefined) return productos;

        return productos.filter(function(productos){
            return productos.nombre.toLowerCase().includes(term.toLowerCase());
        })
    }
}