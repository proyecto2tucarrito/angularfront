import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Marca, MARCAS, Configuracion } from '../models/index';

//const BASE_URL = 'http://localhost:8080/tuCarrito-web/rest/marca';
const BASE_URL = 'https://localhost:8443/tuCarrito-web/rest/marca';

@Injectable()
export class MarcaService {

    private DB = '';
    private locationURL = btoa(location.origin);

    constructor(private http: Http) {
        
    }

    getMarcas() {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
    
        
        //let config : Configuracion = JSON.parse(localStorage.getItem(location.origin));
        //this.DB = config.nombre;

        /*return this.http.get(BASE_URL+'/obtener/'+config.nombre, {headers: headers }).map((response: Response) => {
            
            let marcasResp: Marca[] = response.json();

            return marcasResp;
        
        });*/

        //return this.http.get(BASE_URL+'/obtener/'+this.DB, {headers: headers }).map((response: Response) => {
        return this.http.get(BASE_URL+'/obtener/'+this.locationURL, {headers: headers }).map((response: Response) => {    
             return response.json();
            
        });

        //return Promise.resolve(marcasRet);
        
    }

    getMarca(id) {
        //return this.getMarcas().then(marcas => marcas.find(marca => marca.id === id))
    }

}