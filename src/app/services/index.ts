import {CategoriaService} from './categoria.service';
import {ProductoService} from './producto.service';
import {MarcaService} from './marca.service';
import {MyGlobalService} from '../global';
import {AlertService} from './alert.service';
import {AuthenticationService} from './authentication.service';
import {ClienteService} from './cliente.service';
import {CompraService} from './compra.service';
import {ConfigurationService} from './configuration.service';
import {SocketService} from './socket.service';
import {CarroService} from './carro.service';
import {UploadImageService} from './upload.image.service';

export const SERVICES =  [
    CategoriaService,
    ProductoService,
    MarcaService,
    MyGlobalService,
    AlertService, 
    AuthenticationService,
    ClienteService,
    CompraService,
    ConfigurationService,
    SocketService,
    CarroService,
    UploadImageService
]