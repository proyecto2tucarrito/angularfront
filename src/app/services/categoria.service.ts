import { Injectable } from '@angular/core';
import { CATEGORIAS } from '../models/categoria/categoria-data';
import { Categoria } from '../models/categoria/Categoria'; 
import { Observable, Subject } from 'rxjs';

import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Configuracion } from '../models/index';

//const BASE_URL = 'http://localhost:8080/tuCarrito-web/rest/categoria';
const BASE_URL = 'https://localhost:8443/tuCarrito-web/rest/categoria';

@Injectable()
export class CategoriaService {

    private DB = '';
    private locationURL = btoa(location.origin); 

    constructor(private http: Http) {
        
    }

    getCategorias() {
        //return Promise.resolve(CATEGORIAS)

        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        return this.http.get(BASE_URL+'/obtener/'+this.locationURL, {headers: headers }).map((response: Response) => {    
             return response.json();
            
        });

    }

    getCategoria(id) {
        //let config : Configuracion = JSON.parse(localStorage.getItem(location.origin));
        //this.DB = config.nombre;

        //return this.getCategorias().then(categorias => categorias.find(categoria => categoria.id == id))
    }

}