import { Injectable } from '@angular/core';
import { PRODUCTOS } from '../models/producto/producto-data';
import { Producto } from '../models/producto/Producto'; 
import { LineasCompra } from '../models/linea_compra/LineasCompra'; 
import { Observable, Subject } from 'rxjs';
import { FormArray } from '@angular/forms';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Marca, MARCAS, Configuracion } from '../models/index';

//const BASE_URL = 'http://localhost:8080/tuCarrito-web/rest/producto';
const BASE_URL = 'https://localhost:8443/tuCarrito-web/rest/producto';

@Injectable()
export class ProductoService {

    private DB = '';
    private locationURL = btoa(location.origin);
    productos : Producto[];
    marcas: Marca[];
    lineasCompra : LineasCompra[];

    constructor(private http: Http) {
        
    }

    getProductos() {
        return Promise.resolve(PRODUCTOS)
    }

    getProducto(id) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        return this.http.get(BASE_URL+'/obtenerProducto/'+this.locationURL+'/'+id, {headers: headers }).map((response: Response) => {    
             return response.json();
            
        })
    }

    getComentariosByProducto(id) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        return this.http.get(BASE_URL+'/obtenerComentarios/'+this.locationURL+'/'+id, {headers: headers }).map((response: Response) => {    
             return response.json();
            
        })
    }

    getProductosPorCategoria(id) {
        //return this.productos.filter(producto => producto.categoria.id === id)
        return this.getProductos().then(productos => productos.filter(producto => producto.categoria.id == id))
    }

    getProductosPorTerm(term) {
        //return this.productos.filter(producto => producto.nombre.toLocaleLowerCase().includes(term.toLocaleLowerCase()))
        return this.getProductos().then(productos => productos.filter(producto => producto.nombre.toLocaleLowerCase().includes(term.toLocaleLowerCase())))
    }

    getProductosPorPrecio(precioEntrada, precioSalida) {
        return this.productos.filter(producto => producto.precio >= precioEntrada && producto.precio <= precioSalida)
    }

    getProductosPorValoracion(valoracion) {
        return this.productos.filter(producto => producto.categoria.id === valoracion)
    }

    getProductosPorMarca(marcaArray: Marca[]) {
        return Promise.resolve(this.productos.filter(producto => marcaArray.some(f => f.id == producto.marca.id)))
        //return this.productos.filter(producto => marcaArray.some(f => f.id == producto.marca.id))
    }

    getProductosFromServer(){
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Accept', 'application/json');
        return this.http.get(BASE_URL+'/obtener/'+this.locationURL, {headers: headers }).map((response: Response) => {    
             return response.json();
            
        })
    }

    getProductosPorFilter(idCategoria, termBuscador, precioEntrada, precioSalida, 
        valoracion, marcaArray: string[], data, lineasCompraData){ //Al hacer la request es solo enviar los datos, se hace todo en la logica
           
            //this.productos = PRODUCTOS;
            this.productos = data;
            this.lineasCompra = lineasCompraData;
            this.marcas = [];

            if(idCategoria != 0){
                this.productos = this.productos.filter(producto => producto.categoria.id == idCategoria)
            }
            if(termBuscador != ""){
                this.productos = this.productos.filter(producto => producto.nombre.toLocaleLowerCase().includes(termBuscador.toLocaleLowerCase()))
            }

            if(precioEntrada != 0 && precioSalida != 0){
                this.productos = this.productos.filter(producto => producto.precio >= precioEntrada && producto.precio <= precioSalida)
            }

            if(valoracion != 0){
                if(this.lineasCompra){
                    let prods : Producto[] = [];
                    let prod : Producto;
                    let cantTotal : number;
                    let puntajeTotal : number;
                    let puntaje : number;

                    this.productos.forEach((item, index) => {
                        
                        this.lineasCompra.filter(linea => linea.producto.id == item.id).forEach((item, index) => {
                            
                            if(item){
                                cantTotal = cantTotal+1;
                                puntajeTotal = puntajeTotal + item.comentario.puntuacion;
                            }
                                
                        });
                            
                        puntaje = puntajeTotal/cantTotal;
                        if(puntaje >= valoracion && puntaje < valoracion+1){
                            prods.push(item);
                        }
                        cantTotal = 0;
                        puntajeTotal = 0;
                        puntaje = 0;
                    });

                    if(prods){
                        this.productos = this.productos.filter(producto => prods.some(f => f.id == producto.id))
                    }
                }
            }

            if(marcaArray.length != 0){
                for(let id of marcaArray){
                    if(id){
                        this.marcas.push(MARCAS.find(marca => marca.id == parseInt(id)))
                        
                    }
                }
                
                this.productos = this.productos.filter(producto => this.marcas.some(f => f.id == producto.marca.id))
                       
            }

            //return  this.getProductosPorMarca(this.marcas);

            return Promise.resolve(this.productos);
                    
    }

}