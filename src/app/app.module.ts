import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
//import { MatSliderModule } from '@angular/material';
import { AppRoutingModule } from './app-routing.module';


import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { COMPONENTS } from './components';
import { SERVICES } from './services';
import { AuthGuardLogin } from './_guards/auth.guard';
import { FilterPipe } from './store/filters/search.filter';
import { IonRangeSliderModule } from "ng2-ion-range-slider";

import { AlertModule } from 'ngx-bootstrap';
import { AuthService } from 'angular2-google-login';
import { GoogleSignInComponent } from 'angular-google-signin';
import { ImageUploadModule } from "angular2-image-upload";
import { FancyImageUploaderModule } from 'ng2-fancy-image-uploader';
import { DataTableModule } from "angular2-datatable";

import { RatingModule } from "ng2-rating";
//import { Ng2SliderComponent } from 'ng2-slider-component/ng2-slider.component';
//import { SlideAbleDirective } from 'ng2-slideable-directive/slideable.directive';
//import { Ng2StyledDirective } from 'ng2-styled-directive/ng2-styled.directive';
//"ng2-slideable-directive": "^1.0.13",
//"ng2-slider-component": "^1.0.9",
//"ng2-styled-directive": "^1.0.5",
//import { reducers, metaReducers } from './store/reducers';
//import { AllEffects } from './store/effects';

// used to create fake backend
import { fakeBackendProvider } from './_helpers/index';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';


@NgModule({
  declarations: [
    AppComponent,
    COMPONENTS, 
    FilterPipe,
    GoogleSignInComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    //MaterialModule,
    AppRoutingModule,
    NgxPaginationModule,
    IonRangeSliderModule,
    AlertModule.forRoot(),
    ImageUploadModule.forRoot(),
    FancyImageUploaderModule,
    DataTableModule,
    RatingModule
    
    /*StoreModule.forRoot(reducers, 
    ),
    // StoreDevtoolsModule.instrument({
    //   maxAge: 25 //  Retains last 25 states
    // }),
    AllEffects,*/
  ],
  providers: [
    SERVICES, 
    AuthGuardLogin,
    AuthService,
    // providers used to create fake backend
    fakeBackendProvider,
    MockBackend,
    BaseRequestOptions
],
  bootstrap: [AppComponent]
})
export class AppModule { }
