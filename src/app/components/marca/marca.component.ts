import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AlertService } from '../../services/alert.service';

import { Marca } from '../../models/marca/Marca';
import { MarcaService } from '../../services/marca.service';
import { MyGlobalService } from '../../global';

@Component({
  selector: 'marca',
  templateUrl: './marca.component.html',
  styleUrls: ['./marca.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class MarcaComponent {
  
  marcas:Marca[];
  public marcaChecked:boolean;
  myForm : FormGroup ;

  constructor (private marcaService:MarcaService, private router:Router, private fb: FormBuilder, 
    private myGlobalService:MyGlobalService, private alertService: AlertService) {

  }

  getMarcaData() {  
    this.marcaService.getMarcas().subscribe(
      data => {
          this.marcas = data;
      },
      error => {
          
          this.alertService.error(error);
      });   
     //this.marcaService.getMarcas().then(marcas => this.marcas = marcas)
  }

  onChangeMarca(id:number, isChecked: boolean) {
    const marcaFormArray = <FormArray>this.myForm.controls.idMarca;
  
    if(isChecked) {
        marcaFormArray.push(new FormControl(id));
        this.myGlobalService.getMarcArray().push(id.toString());
    } else {
      let index = marcaFormArray.controls.findIndex(x => x.value == id)
      marcaFormArray.removeAt(index);

      let index2: number = this.myGlobalService.getMarcArray().indexOf(id.toString());
      if (index !== -1) {
        this.myGlobalService.getMarcArray().splice(index2, 1);
      }
    }

    this.router.navigate(['/filtro'], { queryParams: { from: "marca", marcas: marcaFormArray.value } });
    //this.router.navigate(['/filtro'], { queryParams: { marcas: marcaFormArray.value } });

    //this.router.navigate(['/filtro']);
  }

  ngOnInit() {
    this.getMarcaData();

    this.myForm = this.fb.group({
        idMarca: this.fb.array([])
      });
  }

}