import { Component, Input, NgZone } from '@angular/core';
import { Router } from '@angular/router';
//import { Http, Headers, RequestOptions, Response } from '@angular/http';
//import { ProductService } from '../../services/product.service';
//import { CartAction } from 'app/store/actions/cart.actions';
import { MyGlobalService } from '../../global';
//import { Features } from '../../models/index';
import { AuthenticationService } from '../../services/authentication.service';
import { GoogleSignInSuccess } from 'angular-google-signin';
import { AuthService } from 'angular2-google-login';

import { CompraService } from '../../services/compra.service';
import { Cliente, Configuracion } from '../../models/index';
import { CarroService } from '../../services/carro.service';
import { ConfigurationService } from '../../services/configuration.service';

@Component({
  moduleId: module.id,
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavBarComponent {

  @Input('term') term: string;
  @Input() user_name: string;
  showNavBar: boolean = false;
  //featureList: Features[] = [];
  isActive : boolean = false;
  isDisabled : boolean = true;
  totalCantidad: number;
  urlImage : string;

  constructor(private router: Router, private myGlobalService:MyGlobalService, private authenticationService: AuthenticationService,
    private auth: AuthService,
    private compraService: CompraService,
    private zone: NgZone,
    private carroService: CarroService,
    private configurationService: ConfigurationService
  /*private eventGoogle:GoogleSignInSuccess*/) {

    if(this.myGlobalService.showNavBar){
    this.myGlobalService.showNavBar.subscribe((mode: any) => {
        this.showNavBar = true;
      });
    }

    if(this.myGlobalService.hideNavBar){
      this.myGlobalService.hideNavBar.subscribe((mode: any) => {
          this.showNavBar = false;
      });
    }

    if(authenticationService.authenticated())
    {
      this.showNavBar = true;
    }
    else
    {
      this.showNavBar = false;
    }

    if(this.myGlobalService.user_name){
      this.myGlobalService.user_name.subscribe((mode: string) => {
          this.user_name = mode;
      });
    }

    let user: Cliente = JSON.parse(localStorage.getItem('currentUser'));
    if(user){
      this.user_name = user.nombre;
    }
  }

  /*private getFeatureListByLoggedInUser(userID: number): Promise<Features[]> {
      return this.http.get(your api url + '/Feature/GetFeatureListByUserID?userID=' + userID)
          .toPromise()
          .then(response => response.json() as Features[])
          .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
      console.error('An error occurred', error); // for demo purposes only
      return Promise.reject(error.message || error);
  }*/

  eventHandler(event) {
    if(event.keyCode == 13){
      if(this.term == undefined){
        this.myGlobalService.setTermino("");
        this.router.navigate([''])
      }
      else{
        this.myGlobalService.setTermino(this.term);
        this.router.navigate(['/filtro'], { queryParams: { from: "buscador", term: this.term } });
        //this.router.navigate(['/filtro/'+this.term]);
      }
    }
  }

  onClick(value, event) {
    event.preventDefault();
    if(value == undefined){
      this.myGlobalService.setTermino("");
      this.router.navigate(['/filtro'], { queryParams: { from: "buscador" } });
    }
    else{
      this.myGlobalService.setTermino(value);
      this.router.navigate(['/filtro'], { queryParams: { from: "buscador", term: value } });
    }
  }

  onClickDrop(event){
    event.preventDefault();
    if(this.isActive){
      this.isActive = false;
      this.isDisabled = true;
    }
    else{
      this.isActive = true;
      this.isDisabled = false;
    }
  }

  onClickLogout(event){
    event.preventDefault();
    this.authenticationService.logout();

    /*if(this.myGlobalService.googleUser){
      this.myGlobalService.googleUser.disconnect();
    }*/
    let scopeReference = this;
    this.auth.userLogout(function () {
      scopeReference.clearLocalStorage();
    });
    this.router.navigate(['']);
  }

  clearLocalStorage() {
    localStorage.removeItem('token');
    localStorage.removeItem('image');
    localStorage.removeItem('name');
    localStorage.removeItem('email');
}

  loadImage(){

    this.configurationService.getConfiguration().subscribe(
      data => {
        localStorage.setItem(location.origin, JSON.stringify(data));
        let config : Configuracion = data;
        this.urlImage = config.imagen_logo;
      },
      error => {
      });
    /*let config : Configuracion = JSON.parse(localStorage.getItem(location.origin));

    this.urlImage = config.imagen_logo;*/
  }

  ngOnInit() {
    this.totalCantidad = this.carroService.getCantidadTotal();
    
    this.loadImage();

    if(this.carroService.showTotalCant){
      this.carroService.showTotalCant.subscribe((mode: any) => {
        this.totalCantidad = this.carroService.getCantidadTotal();
      });
    }

  }
  
}
