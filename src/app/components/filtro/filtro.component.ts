import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import {IonRangeSliderComponent} from "ng2-ion-range-slider";
import { MyGlobalService } from '../../global';
import { Router } from '@angular/router';

@Component({
  selector: 'filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FiltroComponent {

  @ViewChild('advancedSliderElement') advancedSliderElement: IonRangeSliderComponent;

  constructor (private router:Router, private myGlobalService:MyGlobalService) {

  }

  advancedSlider = {name: "Advanced Slider", onUpdate: undefined, onFinish: undefined};
  
  finish(slider, event) {

    this.myGlobalService.setPrecioEntrada(event.from);
    this.myGlobalService.setPrecioSalida(event.to);
    slider.onFinish = event;
    this.router.navigate(['/filtro'], { queryParams: { from: "precio", precioFrom: event.from, precioTo: event.to } });
  }

  filtroValoracion(event, val){
    event.preventDefault();

    this.myGlobalService.setValoracion(val);
    this.router.navigate(['/filtro'], { queryParams: { from: "valoracion", val: val } });
  }
}