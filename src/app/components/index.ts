//import {CartComponent} from './cart/cart.component';
import {NavBarComponent} from './navbar/navbar.component';
import {CategoriaComponent} from './categoria/categoria.component';
import {ProductosComponent} from './productos/productos.component';
import {ProductoComponent} from './producto/producto.component';
import {FiltroComponent} from './filtro/filtro.component';
import {MarcaComponent} from './marca/marca.component';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {AlertComponent} from './alert/alert.component';
import {PerfilComponent} from './perfil/perfil.component';
import {ComprasComponent} from './compras/compras.component';
import {CompraComponent} from './compra/compra.component';
import {CarroComponent} from './carro/carro.component';
import {ProductoDetailComponent} from './producto-detail/producto-detail.component';
import {ComentariosComponent} from './comentarios/comentarios.component';
import {CompraFinalComponent} from './compra-final/compra-final.component';
import {ComentarioComponent} from './comentario/comentario.component';
//import {ProductComponent} from './product/product.component';
//import {ProductDetailComponent} from './product-detail/product-detail.component';

export const COMPONENTS = [
    //CartComponent,
    //NavBarComponent,
    //ProductComponent,
    //ProductDetailComponent
    NavBarComponent,
    CategoriaComponent,
    ProductosComponent,
    ProductoComponent, 
    FiltroComponent,
    MarcaComponent,
    RegisterComponent, 
    LoginComponent,
    AlertComponent,
    PerfilComponent,
    ComprasComponent, 
    CompraComponent,
    CarroComponent,
    ProductoDetailComponent,
    ComentariosComponent,
    CompraFinalComponent,
    ComentarioComponent
]