import { Component, ViewEncapsulation, Input } from '@angular/core';
import { FormArray } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

//import { Producto } from '../../models/producto/Producto';
import { Producto, Cliente, LineasCompra } from '../../models/index';
import { ProductoService } from '../../services/producto.service';
import { CompraService } from '../../services/compra.service';
import { MyGlobalService } from '../../global';

import { AlertService } from '../../services/alert.service';
import { SocketService } from '../../services/socket.service';
import { CarroService } from '../../services/carro.service';

//import { Categoria } from '../../models/categoria/Categoria';


@Component({
  selector: 'producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ProductoComponent {
    productos:Producto[];
    lineasCompra:LineasCompra[];
    buttonDef:boolean;
    prodShowButton:string[] = [];
    cantidad :number[] = [];
    puntaje :number[] = [];
    puntajeTotal: number = 0;
    cantTotal: number = 0;

  constructor (private productoService:ProductoService, private route:ActivatedRoute, private myGlobalService:MyGlobalService,
    private alertService: AlertService, private socket: SocketService, private router: Router, private carroService: CarroService, 
    private compraService: CompraService) {

  }

  getProductoData() {  
      
    this.route.queryParams.forEach(queryParam => {

        this.productoService.getProductosFromServer().subscribe(
            data => {

                //this.productos = data;
                if(this.myGlobalService.getValoracion() != 0)
                {
                    this.compraService.getAllLineasCompra().subscribe(
                        data2 => {
                                this.lineasCompra = data2;

                                this.productoService.getProductosPorFilter(this.myGlobalService.getCategoria(), this.myGlobalService.getTermino(),
                                        this.myGlobalService.getPrecioEntrada(), this.myGlobalService.getPrecioSalida(), 
                                        this.myGlobalService.getValoracion(), this.myGlobalService.getMarcArray(), data, this.lineasCompra
                                        ).then(productos => this.productos = productos);
                                        
                                this.putComentarios();
                        },
                        error => {
                            this.alertService.error(error);
                        });
                }
                else
                {
                    this.productoService.getProductosPorFilter(this.myGlobalService.getCategoria(), this.myGlobalService.getTermino(),
                            this.myGlobalService.getPrecioEntrada(), this.myGlobalService.getPrecioSalida(), 
                            this.myGlobalService.getValoracion(), this.myGlobalService.getMarcArray(), data, []
                            ).then(productos => this.productos = productos);

                    this.putComentarios();                            
                }
                
            },
            error => {
                
                console.log(error);
                //this.alertService.error(error);
            });

    });
   
  }

  putComentarios(){
    this.compraService.getAllLineasCompra().subscribe(
        data => {
                this.lineasCompra = data;

                this.productos.forEach((item, index) => {
                    
                    this.lineasCompra.filter(linea => linea.producto.id == item.id).forEach((item, index) => {
                        
                        if(item){
                            this.cantTotal = this.cantTotal+1;
                            this.puntajeTotal = this.puntajeTotal + item.comentario.puntuacion;
                        }
                            
                    });
                        
                    this.puntaje[index] = this.puntajeTotal/this.cantTotal;
                    this.cantTotal = 0;
                    this.puntajeTotal = 0;
                });
                
                
        },
        error => {
            this.alertService.error(error);
        });
  }

  changeButtonDef(event, id, stock, index, producto){
      event.preventDefault();
      let userStore : Cliente = JSON.parse(localStorage.getItem('currentUser'));

      if(userStore)
      {
          if(stock != 0){
            this.buttonDef = false;
            this.prodShowButton.push(id);
    
            let cantStock = stock-1;
            this.cantidad[index] = 1;
            this.socket.send(id+','+1+',-');
            this.carroService.addItem(producto);
          }
      }
      else
      {
        this.router.navigate(['/login']);
      }
  }

  plusProduct(event, id, stock, index, producto){
    event.preventDefault();

    if(stock != 0){
        let cantStock = stock-1;
        this.cantidad[index]++;
        this.socket.send(id+','+1+',-');
        this.carroService.addItem(producto);
    }
  }

  minusProduct(event, id, stock, index, producto){
    event.preventDefault();

    let cantStock = parseInt(stock)+1;
    if(this.cantidad[index] == 1)
    {
        this.buttonDef = true;
        this.prodShowButton.splice(index,1);
        this.carroService.removeAllItem(producto);
    }
    else{
        this.carroService.removeItem(producto);
    }
    this.cantidad[index]--;
    this.socket.send(id+','+1+',+');
   
  }

  hasProduct(id, index){
      /*if(this.prodShowButton){
        return this.prodShowButton.indexOf(id) >=0;
      }
      else{
          return false;
      }*/

      this.cantidad[index] = this.carroService.getCantidad(id);
      if(this.cantidad[index] == 0){
          return false;
      }
      else{
          return true;
      }
   
  }

  public ngOnInit() {

    this.getProductoData();
    this.buttonDef = true;

    this.socket.getEventListener().subscribe(event => {
        if(event.type == "message") {
            /*let data = event.data.content;
            if(event.data.sender) {
                data = event.data.sender + ": " + data;
            }*/
            let toArray =  event.data.split(",");

            this.productos.forEach((item, index) => {
                if(item.id == toArray[0]){
                    item.stock = toArray[1];
                    this.carroService.setStockToProducto(item, toArray[1]);
                }
                    
            });
        }
        if(event.type == "close") {
            console.log('Se desconecto');
            localStorage.clear();
            //this.alertService.success('Close');
        }
        if(event.type == "open") {
            console.log('Se conecto'+event);
           // this.alertService.success('Open');
        }
    });

    
  }

}