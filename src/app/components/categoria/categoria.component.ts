import { Component, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import { Categoria } from '../../models/categoria/Categoria';
import { CategoriaService } from '../../services/categoria.service';
import { MyGlobalService } from '../../global';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss'],
  encapsulation: ViewEncapsulation.None
  
})

export class CategoriaComponent {
  
  categorias:Categoria[];


  constructor (private categoriaService:CategoriaService, private myGlobalService:MyGlobalService, private router:Router, 
    private alertService: AlertService) {

  }

  onClick(value, event) {
    event.preventDefault();
    this.myGlobalService.setCategoria(value);
    //this.router.navigate(['/categoria/'+value]);
    this.router.navigate(['/filtro'], { queryParams: { from: "categoria", cat: value } });
  }

  getCategoriaData() {     
    this.categoriaService.getCategorias().subscribe(
      data => {
          this.categorias = data;
      },
      error => {
          
          this.alertService.error(error);
      });   
     //this.categoriaService.getCategorias().then(categorias => this.categorias = categorias)
  }

  ngOnInit() {
    // Get initial data from productService to display on the page
    this.getCategoriaData();
  }

}