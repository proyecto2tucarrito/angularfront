import {Categoria} from './Categoria';

export const CATEGORIAS:Categoria[] = [
    { id: 1, nombre: 'Alimentos'},
    { id: 2, nombre: 'Vestimenta'},
    { id: 3, nombre: 'Tecnologia'},
    { id: 4, nombre: 'Accesorios'},
]