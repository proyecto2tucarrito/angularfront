import {Categoria} from '../categoria/Categoria';
import {Imagen} from '../imagen/Imagen';
import {Marca} from '../marca/Marca';
//export enum Moneda {$,USD,EU}

export class Producto {
    id:number;
    nombre:string;
    descripcion:string;
    precio:number;
    precioConDcto: number;
    //moneda:Moneda;
    moneda:string;
    descuento:number;
    stock:number;
    activo:boolean;
    marca:Marca;
    categoria:Categoria;
    imagenes:Imagen[];
}