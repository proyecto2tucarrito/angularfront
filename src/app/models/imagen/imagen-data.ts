import {Imagen} from './Imagen';

export const IMAGENES:Imagen[] = [
    { id: 1, imagen: '/assets/img/phones/nexus-s.0.jpg'},
    { id: 2, imagen: '/assets/img/phones/dell-venue.0.jpg'},
    { id: 3, imagen: '/assets/img/phones/droid-2-global-by-motorola.0.jpg'},
    { id: 4, imagen: '/assets/img/phones/droid-pro-by-motorola.0.jpg'},
    { id: 5, imagen: '/assets/img/phones/motorola-bravo-with-motoblur.0.jpg'},
    { id: 6, imagen: '/assets/img/phones/motorola-defy-with-motoblur.0.jpg'},
    { id: 7, imagen: '/assets/img/phones/pasos.png'},
]