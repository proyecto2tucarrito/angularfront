import {Marca} from './Marca';

export const MARCAS:Marca[] = [
    { id: 1, nombre: 'Nike'},
    { id: 2, nombre: 'Adidas'},
    { id: 3, nombre: 'Samsung'},
    { id: 4, nombre: 'Windows'}
]