/*export var GlobalVariable = Object.freeze({
    marcaArray: [],
    categoria: 0,
    termino: "",
    precioEntrada: 0,
    precioSalida: 0,
    valoracion: 0

});*/
//import {Injectable} from '@angular/core';

import {Injectable, EventEmitter} from '@angular/core';
import {Compra, Configuracion} from './models/index';

@Injectable()
export class MyGlobalService {
    public showNavBar: EventEmitter<any> = new EventEmitter();
    public hideNavBar: EventEmitter<any> = new EventEmitter();
    public user_name: EventEmitter<string> = new EventEmitter();
    public orden_compra: Compra;
    //public showNavBar;
    //public hideNavBar;
    //public navBarClient = false;
    //public googleUser: gapi.auth2.GoogleUser;
    //public googleUser2: EventEmitter<gapi.auth2.GoogleUser> = new EventEmitter();
    private marcArray: string[] = [];
    private categoria = 0;
    private termino = "";
    private precioEntrada = 0;
    private precioSalida = 0;
    private valoracion = 0;

    constructor() {}

    setMarcArray(val) {
        this.marcArray = val;
    }

    getMarcArray() {
        return this.marcArray ;
    }

    setCategoria(val) {
        this.categoria = val;
    }

    getCategoria() {
        return this.categoria ;
    }

    setTermino(val){
        this.termino = val;
    }

    getTermino(){
        return this.termino;
    }

    setPrecioEntrada(val){
        this.precioEntrada = val;
    }

    getPrecioEntrada(){
        return this.precioEntrada;
    }

    setPrecioSalida(val){
        this.precioSalida = val;
    }

    getPrecioSalida(){
        return this.precioSalida;
    }

    setValoracion(val){
        this.valoracion = val;
    }

    getValoracion(){
        return this.valoracion;
    }

}