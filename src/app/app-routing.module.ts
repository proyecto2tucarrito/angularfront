import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AuthGuardLogin } from './_guards/auth.guard';
import { ProductosComponent } from './components/productos/productos.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { ComprasComponent } from './components/compras/compras.component';
import { CompraComponent } from './components/compra/compra.component';
import { CarroComponent } from './components/carro/carro.component';
import { ProductoDetailComponent } from './components/producto-detail/producto-detail.component';
import { ComentariosComponent } from './components/comentarios/comentarios.component';
import { CompraFinalComponent } from './components/compra-final/compra-final.component';
import { ComentarioComponent } from './components/comentario/comentario.component';
//import { ProductDetailComponent } from './components/product-detail/product-detail.component';
//import { CartComponent } from './components/cart/cart.component';

const routes: Routes = [
  {
    path: '',
    component: ProductosComponent
  },
  {
    path: 'categoria/:id',
    component: ProductosComponent
  },
  {
    path: 'filtro/:term',
    component: ProductosComponent
  },
  {
    path: 'filtro',
    component: ProductosComponent
  },
  {
    path: 'productos',
    component: ProductosComponent
  },
  { 
    path: 'login', 
    component: LoginComponent 
  },
  { 
    path: 'register', 
    component: RegisterComponent 
  },
  { 
    path: 'perfil', 
    component: PerfilComponent,
    canActivate: [AuthGuardLogin]
  },
  { 
    path: 'compras', 
    component: ComprasComponent,
    canActivate: [AuthGuardLogin]
  },
  { 
    path: 'compra/:id', 
    component: CompraComponent,
    canActivate: [AuthGuardLogin]
  },
  { 
    path: 'carro', 
    component: CarroComponent,
    canActivate: [AuthGuardLogin]
  },
  { 
    path: 'detail/:id', 
    component: ProductoDetailComponent 
  },
  { 
    path: 'comentarios/:id', 
    component: ComentariosComponent 
  },
  { 
    path: 'final', 
    component: CompraFinalComponent,
    canActivate: [AuthGuardLogin]
  },
  { 
    path: 'comentario/:id/:idLinea', 
    component: ComentarioComponent 
  }/*,
  {
    path: 'cart',
    component: CartComponent
  }*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }